use anyhow::{anyhow, bail, Error};
use clap::Parser;
use comfy_table::presets::UTF8_FULL;
use directories::BaseDirs;
use duct::cmd;
use image::{DynamicImage, Rgba};
use inquire::{
    list_option::ListOption,
    ui,
    ui::{Attributes, RenderConfig, StyleSheet},
    validator::Validation,
    MultiSelect, Select,
};
use knuffel::{
    ast::{Literal, TypeName},
    decode::{Context, Kind},
    errors::{DecodeError, ExpectedType},
    span::Spanned,
    traits::ErrorSpan,
    Decode, DecodeScalar,
};
use rand::prelude::*;
use rusttype::{point, Font, Scale};
use std::ops::Deref;
use std::{collections::BTreeMap, str::FromStr};
use tempfile::NamedTempFile;

const PAGE_SIZE: usize = 20;

#[derive(Debug, Decode, Clone)]
struct Group {
    #[knuffel(argument)]
    name: String,
    #[knuffel(child, unwrap(arguments))]
    servers: Vec<String>,
}

#[derive(Debug, Copy, Clone)]
struct Color {
    r: u8,
    g: u8,
    b: u8,
}

impl<S: ErrorSpan> DecodeScalar<S> for Color {
    fn raw_decode(val: &Spanned<Literal, S>, ctx: &mut Context<S>) -> Result<Color, DecodeError<S>> {
        let default_color = Color { r: 0, g: 0, b: 0 };
        match &**val {
            Literal::Int(i) => {
                let color = match u32::try_from(i) {
                    Ok(color) => color,
                    Err(e) => {
                        ctx.emit_error(DecodeError::conversion(val, e));
                        return Ok(default_color);
                    }
                };
                if color > 0xffffff {
                    let err = DecodeError::conversion(val, anyhow!("Color should be in range 0...0xffffff"));
                    ctx.emit_error(err);
                    return Ok(default_color);
                }
                Ok(Color {
                    r: ((color & 0xff0000) >> 16) as u8,
                    g: ((color & 0x00ff00) >> 8) as u8,
                    b: (color & 0x0000ff) as u8,
                })
            }
            _ => {
                let err = DecodeError::scalar_kind(Kind::Int, val);
                ctx.emit_error(err);
                Ok(default_color)
            }
        }
    }
    fn type_check(type_name: &Option<Spanned<TypeName, S>>, ctx: &mut Context<S>) {
        if let Some(typ) = type_name {
            ctx.emit_error(DecodeError::TypeName {
                span: typ.span().clone(),
                found: Some(typ.deref().clone()),
                expected: ExpectedType::no_type(),
                rust_type: "String",
            });
        }
    }
}

#[derive(Debug, Clone, Decode)]
struct Category {
    #[knuffel(argument)]
    name: String,
    #[knuffel(property)]
    logo_color: Color,
    #[knuffel(property)]
    kitty_theme: String,
}

#[derive(Debug, Decode, Clone)]
struct Server {
    #[knuffel(argument)]
    alias: String,
    #[knuffel(argument)]
    conn: String,
    #[knuffel(argument)]
    category: String,
    #[knuffel(argument)]
    key: Option<String>,
    #[knuffel(property)]
    comment: Option<String>,
}

#[derive(Decode, Debug)]
struct Config {
    #[knuffel(children(name = "category"))]
    categories: Vec<Category>,
    #[knuffel(children(name = "server"))]
    servers: Vec<Server>,
    #[knuffel(children(name = "group"))]
    groups: Vec<Group>,
}

#[derive(Debug, Parser)]
struct Args {
    /// do nothing, just write ssh_config
    #[arg(long)]
    update_ssh_config: bool,
    /// list available servers and categories
    #[arg(short, long)]
    list: bool,
    /// open connection in a new kitty window
    #[arg(short, long)]
    no_window: bool,
    /// launch whole server group in a new kitty tab
    #[arg(short, long)]
    group: bool,
    /// list server completions
    #[arg(long)]
    server_completions: bool,
    /// list group completions
    #[arg(long)]
    group_completions: bool,
    /// Mulitselect
    #[arg(short, long)]
    many: bool,
    alias: Option<String>,
}

type PConfig = BTreeMap<String, (Server, Category)>;

fn process_config(cfg: Config) -> Result<PConfig, Error> {
    let category_hm: BTreeMap<_, _> = cfg.categories.into_iter().map(|g| (g.name.clone(), g)).collect();
    let mut res = BTreeMap::new();
    for server in cfg.servers.into_iter() {
        let category = category_hm
            .get(&server.category)
            .ok_or(anyhow!("Category for server {server:?} does not exist! :sadge:"))?;
        res.insert(server.alias.clone(), (server, category.clone()));
    }
    Ok(res)
}

fn do_list(cfg: &PConfig) {
    let mut table = comfy_table::Table::new();
    table.load_preset(UTF8_FULL);
    for (alias, (server, category)) in cfg.iter() {
        let color = comfy_table::Color::Rgb {
            r: category.logo_color.r,
            g: category.logo_color.g,
            b: category.logo_color.b,
        };
        table.add_row(vec![
            // Cell::new(&category.name).fg(color),
            comfy_table::Cell::new(alias).fg(color),
            comfy_table::Cell::new(&server.conn).fg(color),
            comfy_table::Cell::new(&get_encoded_ip(server)).fg(color),
            comfy_table::Cell::new(server.comment.as_deref().unwrap_or("")).fg(color),
        ]);
    }
    println!("{table}");
}

const S_LAUNCHED_IN_A_NEW_WINDOW: &str = "S_LAUNCHED_IN_A_NEW_WINDOW";

fn get_encoded_ip(s: &Server) -> String {
    let host = if s.conn.contains('@') {
        s.conn.split_once('@').unwrap().1
    } else {
        s.conn.as_str()
    };
    let mut answ = String::new();
    if let Ok(ip) = std::net::Ipv4Addr::from_str(host) {
        for octet in ip.octets() {
            answ.push_str(&format!("{octet:0>2x}"));
        }
    }
    answ
}

fn do_connect(alias: &str, cfg: &PConfig) -> Result<(), Error> {
    let (server, category) = cfg
        .get(alias)
        .ok_or(anyhow!("Server {alias} doesn't even exist :notLikeThis:"))?;
    let logo = gen_logo(alias, category)?;
    cmd!(
        "kitty",
        "@",
        "set-window-logo",
        "-m",
        if std::env::var(S_LAUNCHED_IN_A_NEW_WINDOW).is_ok() {
            format!("title:^{alias}$")
        } else {
            String::from("state:active")
        },
        format!("{}", logo.path().display())
    )
    .run()?;
    let cmd = if let Some(ref i) = server.key {
        cmd!(
            "kitty",
            "+kitten",
            "ssh",
            "--kitten",
            format!("color_scheme={}", category.kitty_theme),
            format!("-i{}", i),
            &server.conn
        )
    } else {
        cmd!(
            "kitty",
            "+kitten",
            "ssh",
            "--kitten",
            format!("color_scheme={}", category.kitty_theme),
            &server.conn
        )
    };
    cmd.run()?;
    Ok(())
}

fn gen_logo(alias: &str, g: &Category) -> Result<NamedTempFile, Error> {
    let height = 64;
    let right_margin = 70;
    let font_data = include_bytes!("../font.ttf");
    let font =
        Font::try_from_bytes(font_data as &[u8]).expect("Could not construct font, something is terribly broken :cry:");
    let scale1 = Scale::uniform(64.0);
    let v_metrics1 = font.v_metrics(scale1);
    let trunc: String = alias.chars().take(10).collect();
    let glyphs: Vec<_> = font.layout(&trunc, scale1, point(0., v_metrics1.ascent)).collect();
    let mut text_width = 0;
    for glyph in glyphs.iter() {
        if let Some(bounding_box) = glyph.pixel_bounding_box() {
            text_width += bounding_box.width();
        }
    }
    let width = text_width as u32 + right_margin + 10;
    let mut res = DynamicImage::new_rgba8(width, height);

    let img = res.as_mut_rgba8().unwrap();
    let base_pixel = Rgba([0, 0, 0, 0]);
    for i in 0..width {
        for j in 0..height {
            img.put_pixel(i, j, base_pixel)
        }
    }
    for glyph in glyphs {
        if let Some(bounding_box) = glyph.pixel_bounding_box() {
            glyph.draw(|x, y, v| {
                img.put_pixel(
                    x + (width - (text_width as u32) - right_margin) + bounding_box.min.x as u32,
                    y + bounding_box.min.y as u32,
                    Rgba([g.logo_color.r, g.logo_color.g, g.logo_color.b, (v * 255.0) as u8]),
                )
            });
        }
    }

    let tmpfile = tempfile::Builder::new().suffix(".png").tempfile()?;
    img.save(tmpfile.path())?;
    Ok(tmpfile)
}

fn do_connect_w(alias: &str, cfg: &PConfig) -> Result<(), Error> {
    let (_server, _category) = cfg
        .get(alias)
        .ok_or(anyhow!("Server {alias} doesn't even exist :notLikeThis:"))?;
    cmd!(
        "kitty",
        "@",
        "launch",
        "--env",
        format!("{}=1", S_LAUNCHED_IN_A_NEW_WINDOW),
        "--type=window",
        format!("--title={}", alias),
        "s",
        "-n",
        alias
    )
    .run()?;
    Ok(())
}

fn do_connect_g(alias: &str, groups: &[Group], cfg: &PConfig) -> Result<(), Error> {
    let mut group = None;
    for g in groups.iter() {
        if g.name == alias {
            group = Some(g);
            break;
        }
    }
    let group = group.ok_or(anyhow!("Could not find group {alias}! :pepeHands:"))?;
    if group.servers.is_empty() {
        bail!("Group {alias} has no servers. Rip.");
    }
    let mut servers = Vec::new();
    for s in group.servers.iter() {
        servers.push(cfg.get(s).ok_or(anyhow!("Could not find server {s} :sadge:"))?);
    }
    do_connect_multiple_servers(&servers, cfg, Some(group.name.as_str()))
}

fn do_connect_multiple_servers(
    servers: &[&(Server, Category)],
    cfg: &PConfig,
    group_name: Option<&str>,
) -> Result<(), Error> {
    let mut tab_name = String::new();
    for s in servers.iter() {
        tab_name.push_str(&format!("{} | ", s.0.alias));
    }
    let tab_id = cmd!(
        "kitty",
        "@",
        "launch",
        "--type=tab",
        "--tab-title",
        group_name.unwrap_or(tab_name.as_str()),
        "s",
        &servers[0].0.alias
    )
    .read()?;
    cmd!("kitty", "@", "goto-layout", "-m", format!("id:{tab_id}"), "grid").run()?;
    for s in &servers[1..] {
        do_connect_w(&s.0.alias, cfg)?;
    }
    // let window_id = cmd!(
    cmd!(
        "kitty",
        "@",
        "launch",
        "--type=window",
        "kitty",
        "+kitten",
        "broadcast",
        "--match-tab",
        "state:active"
    )
    .run()?;
    //.read()?;
    // cmd!(
    //     "kitty",
    //     "@",
    //     "resize-window",
    //     "--match",
    //     format!("id:{window_id}"),
    //     "--axis",
    //     "vertical",
    //     "--increment",
    //     "-30"
    // )
    // .run()?;

    Ok(())
}

const SERVER_PROMPTS: &[&str] = &[
    "🚀 Choose your SSH server destination! Pick from the galaxy of servers. Where will your connection voyage take you today?\n",
    "🔐 Unlock the SSH server vault! Select your key to access the hidden treasures of secure connections.\n",
    "🌐 Connect worldwide! Point your SSH compass and explore servers from all corners of the globe.\n",
    "🏰 Enter the SSH fortress! Pick your castle of choice to fortify your connection.\n",
    "🚁 Elevate your SSH experience! Hover over the server landscape and descend upon your chosen destination.\n",
    "🎯 Aim for the SSH bullseye! Select your target server and hit the mark with precision.\n",
    "🏝️ SSH island-hopping adventure! Pick your tropical SSH paradise to bask in the sun of secure connectivity.\n",
    "🧙‍♂️ Wizardry with SSH! Choose your magical portal to access the mystical realms of serverland.\n",
    "🚢 Set sail for SSH servers! Pick your SSH ship and embark on a voyage across the server seas.\n",
    "🎆 Fireworks of SSH choices! Select your favorite SSH server and let the connections light up your night.\n"
];

const GROUP_PROMPTS: &[&str] = &[
    "🌐 Gather 'round the virtual campfire! Choose your server group to embark on a digital adventure.\n",
    "🏰 Welcome to the server kingdom! Select your faction from the server groups to join the battle for data supremacy.\n",
    "🚀 Blast off to server space! Pick your spacecraft and explore the galaxy of server groups.\n",
    "🌴 Server group paradise! Select your tropical oasis to relax and manage your digital resources.\n",
    "🎯 Bullseye your server goals! Choose the server group that aligns with your mission and hit the mark.\n",
    "🕵️ Secret agent server groups! Pick your undercover identity and dive into the world of covert data operations.\n",
    "🌈 Server group rainbow! Select your color-coded path to organize and access your server resources.\n",
    "🤖 Welcome to the server army! Join the ranks of server groups and prepare for digital warfare.\n",
    "🎆 Fireworks of server choices! Pick your favorite server group and let your digital dreams explode.\n",
    "🚢 Set sail for server islands! Choose your server group and navigate the sea of data with confidence.\n"
];

fn pick_server_prompt() -> &'static str {
    SERVER_PROMPTS.choose(&mut rand::thread_rng()).unwrap()
}

fn pick_group_prompt() -> &'static str {
    GROUP_PROMPTS.choose(&mut rand::thread_rng()).unwrap()
}

fn select_interactive(cfg: &PConfig) -> Result<Vec<String>, Error> {
    let rc = RenderConfig {
        prompt: StyleSheet::new()
            .with_fg(ui::Color::LightMagenta)
            .with_attr(Attributes::BOLD),
        ..Default::default()
    };
    let validator = |a: &[ListOption<&String>]| {
        if a.is_empty() {
            return Ok(Validation::Invalid("Need to select at least one option".into()));
        }
        Ok(Validation::Valid)
    };
    let options: Vec<_> = cfg
        .values()
        .map(|(s, g)| {
            format!(
                "{} | {} | {} | {} | {}",
                s.alias,
                g.name,
                s.conn,
                get_encoded_ip(s),
                s.comment.as_deref().unwrap_or("")
            )
        })
        .collect();
    let servers = MultiSelect::new(pick_server_prompt(), options)
        .with_page_size(PAGE_SIZE)
        .with_validator(validator)
        .with_render_config(rc)
        .prompt()?;
    Ok(servers)
}

fn select_interactive_one(cfg: &PConfig) -> Result<String, Error> {
    let rc = RenderConfig {
        prompt: StyleSheet::new()
            .with_fg(ui::Color::LightCyan)
            .with_attr(Attributes::BOLD),
        ..Default::default()
    };
    let options: Vec<_> = cfg
        .values()
        .map(|(s, g)| {
            format!(
                "{} | {} | {} | {} | {}",
                s.alias,
                g.name,
                s.conn,
                get_encoded_ip(s),
                s.comment.as_deref().unwrap_or("")
            )
        })
        .collect();
    let server = Select::new(pick_server_prompt(), options)
        .with_page_size(PAGE_SIZE)
        .with_render_config(rc)
        .prompt()?;
    Ok(server)
}

fn select_interactive_g(groups: &[Group]) -> Result<Vec<String>, Error> {
    let rc = RenderConfig {
        prompt: StyleSheet::new()
            .with_fg(ui::Color::LightYellow)
            .with_attr(Attributes::BOLD),
        ..Default::default()
    };
    let validator = |a: &[ListOption<&String>]| {
        if a.is_empty() {
            return Ok(Validation::Invalid("Need to select at least one option".into()));
        }
        Ok(Validation::Valid)
    };
    let options: Vec<_> = groups
        .iter()
        .map(|g| {
            let mut res = format!("{} | ", g.name);
            for i in g.servers.iter() {
                res.push_str(&format!("{} | ", i));
            }
            res
        })
        .collect();
    let groups = MultiSelect::new(pick_group_prompt(), options)
        .with_page_size(PAGE_SIZE)
        .with_validator(validator)
        .with_render_config(rc)
        .prompt()?;
    Ok(groups)
}

fn select_interactive_g_one(groups: &[Group]) -> Result<String, Error> {
    let rc = RenderConfig {
        prompt: StyleSheet::new()
            .with_fg(ui::Color::LightRed)
            .with_attr(Attributes::BOLD),
        ..Default::default()
    };
    let options: Vec<_> = groups
        .iter()
        .map(|g| {
            let mut res = format!("{} | ", g.name);
            for i in g.servers.iter() {
                res.push_str(&format!("{} | ", i));
            }
            res
        })
        .collect();
    let group = Select::new(pick_group_prompt(), options)
        .with_page_size(PAGE_SIZE)
        .with_render_config(rc)
        .prompt()?;
    Ok(group)
}

fn do_update_ssh_config(home: &std::path::Path, c: &Config) -> Result<(), Error> {
    let mut ssh_config_contents = String::new();

    for server in c.servers.iter() {
        ssh_config_contents.push_str(&format!("Host {}\n", server.alias,));
        if let Some((user, host)) = server.conn.split_once('@') {
            ssh_config_contents.push_str(&format!("\tHostName {}\n\tUser {}\n", host, user));
        } else {
            ssh_config_contents.push_str(&format!("\t HostName {}", server.conn))
        }
        if let Some(ref k) = server.key {
            ssh_config_contents.push_str(&format!("\tIdentityFile {}\n", k));
        }
    }
    let ssh_config_path = home.join(".ssh/config-by-s");
    std::fs::write(&ssh_config_path, &ssh_config_contents)?;
    println!("Wrote ssh config to {}", ssh_config_path.display());
    println!("Add `Include ~/.ssh/config-by-s` line to your main ssh config file");
    Ok(())
}

fn main() -> Result<(), Error> {
    let args = Args::parse();
    let base_dirs = BaseDirs::new().ok_or(anyhow!("No home directory :pepeHands:"))?;
    let config_dir = base_dirs.config_dir();
    let config: Config = match knuffel::parse(
        "servers.kdl",
        &std::fs::read_to_string(&config_dir.join("servers.kdl"))?,
    ) {
        Ok(c) => c,
        Err(e) => {
            println!("{:?}", miette::Report::new(e));
            bail!("Could not parse config!");
        }
    };
    let groups = config.groups.clone();

    if args.update_ssh_config {
        do_update_ssh_config(&base_dirs.home_dir(), &config)?;
        return Ok(());
    }

    let processed_config = process_config(config)?;
    if args.list {
        do_list(&processed_config);
        return Ok(());
    }
    if args.server_completions {
        for alias in processed_config.keys() {
            println!("{alias}");
        }
        return Ok(());
    }
    if args.group_completions {
        for group in groups {
            println!("{}", group.name);
        }
        return Ok(());
    }
    if let Some(alias) = args.alias.as_ref() {
        if args.no_window {
            do_connect(alias, &processed_config)?;
        } else if args.group {
            do_connect_g(alias, &groups, &processed_config)?;
        } else {
            do_connect_w(alias, &processed_config)?;
        }
    } else if args.group {
        if args.many {
            let grps = select_interactive_g(&groups)?;
            for g in grps {
                do_connect_g(g.split('|').next().unwrap().trim(), &groups, &processed_config)?;
            }
        } else {
            let group = select_interactive_g_one(&groups)?;
            do_connect_g(group.split('|').next().unwrap().trim(), &groups, &processed_config)?;
        }
    } else if args.many {
        let selected = select_interactive(&processed_config)?;
        let mut servers = Vec::new();
        for s in selected.into_iter() {
            let alias = s.split('|').next().unwrap().trim();
            servers.push(
                processed_config
                    .get(alias)
                    .ok_or(anyhow!("Could not find server {alias} :sadge:"))?,
            );
        }
        do_connect_multiple_servers(&servers, &processed_config, None)?;
    } else {
        let server = select_interactive_one(&processed_config)?;
        do_connect_w(server.split('|').next().unwrap().trim(), &processed_config)?;
    }

    Ok(())
}
