# S

simple ssh connection manager designed to run in [kitty](https://sw.kovidgoyal.net/kitty/) terminal

## features
- list of servers is managed in single central config file (`~/.config/servers.toml`)
- each server can have category (category defines kitty theme and logo color). Useful to distinguish production servers from staging and development ones.
- server groups allow to launch multiple connections at once and lay them out nicely using kitty grid laoyut. Also automatically start kitty broadcast plugin.
- fish completions

## build
- Install latest rust toolchain: https://rustup.rs
- Checkout this repo and cd to workdir
- Pick a font of your choice for logo and place it in the root of the repo working dir.
- run `cargo build --release`
- copy `target/release/s` file somewhere to your `$PATH`

## usage

Example `~/.config/servers.toml` file
```toml
[[groups]]
name = "prod"
servers = ["prod1", "prod2"]

[[groups]]
name = "staging"
servers = ["staging1", "staging2", "staging3"]

[[categories]]
name = "prod"
logo_color = [216, 26, 57]
kitty_theme = "Red Alert"

[[categories]]
name = "staging"
logo_color = [57, 216, 26]
kitty_theme = "Grass"

[[servers]]
name="prod1"
category="prod"
identity_file="/path/to/identity.pem"
conn="root@prod.example.com"

[[servers]]
name="staging1"
category="staging"
# identity file is optional
conn="root@staging.example.com"
```

### list servers

``` sh
$ s
```

### launch ssh session in the current kitty window

``` sh
$ s -n prod1
```

### launch ssh session in a new kitty window

``` sh
$ s staging1
```

### launch group of ssh sessions in a new kitty tab

``` sh
$ s -g prod
```

## Install completiotions
At this moment only supported for fish shell:

``` sh
$ cp completions.fish ~/.config/fish/completions/s.fish
```
