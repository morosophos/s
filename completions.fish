#!/usr/bin/env fish

# copy this file to your ~/.config/fish/completions/

complete -f -c s -n "not __fish_contains_opt -s g group" -a "(s --server-completions)"
complete -f -c s -n "__fish_contains_opt -s g group" -a "(s --group-completions)"
